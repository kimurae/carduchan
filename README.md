# CARDUCHAN
The search and browse engine for Weiss Schwartz cards.

## Warning
This site relies on writing a data import/webscraper tool. As this was built mostly to help me with a personal hobby of mine, I never needed, nor added any forms to write data. Again this tool is for searching already existing data previously inserted into the database. I put the code here for informational purposes only.

## Installation

*NOTE* This site uses postgres, I would recommend for MacOS users to install Postgres.app

```bash
git clone git@gitlab.com:kimurae/carduchan.git
bundle
rails db:setup
rails db:schema:load
```

Then acquire the data for weiss schwartz cards, and write a tool to import them into the database.

## Usage

```bash
foreman start
```

## Objective

Just a fun site made in my spare time to help me look over the various cards available for
a new card game I just got into.

## Contributing

This is just a private project of mine.

