
environment ENV.fetch('RACK_ENV', 'development' )
port        ENV.fetch('PORT', 3000)
threads     *Array.new(2, ENV.fetch('RAILS_MAX_THREADS', 5).to_i)
workers     ENV.fetch('WEB_CONCURRENCY', 2 ).to_i

preload_app!

on_worker_boot do
  ActiveRecord::Base.establish_connection
end

plugin :tmp_restart
