Rails.application.routes.draw do

  namespace :api do
    resources :card_facets, only: :index

    resources :search, only: :index

    resources :series, only: :index do
      resources :cards, only: :index
    end
  end

  resources :series, only: :index do
    resources :cards, only: :index
  end

  resources :search, only: %i(new create)

  root 'series#index'
end
