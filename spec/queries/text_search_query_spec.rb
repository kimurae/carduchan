require 'rails_helper'

RSpec.describe TextSearchQuery do

  describe '#call' do

    let(:valid_sql) do
      %q(SELECT "documents".* FROM "documents" WHERE ("documents"."document" @@ plainto_tsquery('english', 'test')) ORDER BY ts_rank("documents"."document", plainto_tsquery('english', 'test')) DESC)
    end

    it 'should generate a correct SQL query' do
      expect(TextSearchQuery.new(relation: Document, column: :document).call('test').to_sql).to eq valid_sql
    end
  end
end
