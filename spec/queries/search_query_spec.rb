require 'rails_helper'

RSpec.describe SearchQuery do

  describe '#results' do

    let(:valid_params) do
      {
        card_text: 'My Little Pony',
        card_trait: ['Unicorn', 'Magic'],
        card_rarity: ['U'],
        card_expansion: [1],
        metadata: {
          card_type: ['Character'],
          color: ['Yellow', 'Red'],
          level: ['0'],
          power: ['2500'],
          cost: ['1']
        }
      }
    end

    let(:valid_sql) do
      %q(SELECT "cards".* FROM "cards" INNER JOIN "documents" ON "documents"."card_id" = "cards"."id" WHERE ("documents"."document" @@ plainto_tsquery('english', 'My Little Pony')) AND "cards"."expansion_id" = 1 AND "cards"."metadatum_id" IN (SELECT "metadata"."id" FROM "metadata" WHERE "metadata"."card_type" = 'Character' AND "metadata"."color" IN ('Yellow', 'Red') AND "metadata"."level" = 0 AND "metadata"."power" = 2500 AND "metadata"."cost" = 1) AND "cards"."id" IN (SELECT "printings"."card_id" FROM "printings" WHERE "printings"."rarity" = 'U') AND ("cards"."traits" @> ARRAY['Unicorn','Magic']::varchar[]) ORDER BY ts_rank("documents"."document", plainto_tsquery('english', 'My Little Pony')) DESC)
    end

    it 'should generate a correct SQL query' do
      expect(SearchQuery.call(valid_params).to_sql).to eq valid_sql
    end
  end
end
