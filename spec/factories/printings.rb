FactoryGirl.define do
  factory :printing do
    image_url "MyString"
    number "MyString"
    rarity "MyString"
  end
end
