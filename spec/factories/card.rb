FactoryGirl.define do

  factory :card do

    image_url '/'

    trait :character do
      name 'Town Leader, Starlight Glimmer'
      first_printing_number 'MP/S42-E103'
      printings {
        [
          Printing.new( number: 'MP/S42-E103', rarity: 'U' ),
          Printing.new( number: 'MP/S42-TE01', rarity: 'TD' )
        ]
      }
      text '[AUTO] All other characters lose all printed abilities.'
      traits { %w(unicorn magic) }
      card_trigger { { soul: 2 } }
      expansion { { name: 'My Little Pony' } }
      metadatum {
        { card_type: 'Character', color: 'yellow', cost: 2, level: 3, power: 10000, soul: 3 }
      }
    end

    trait :event do
      name 'Crystal Faire'
      first_printing_number 'MP/S42-E102'
      printings {
        [
          Printing.new( number: 'MP/S42-E102', rarity: 'RRR')
        ]
      }
      expansion { { name: 'My Little Pony' } }
      metadatum {
        { card_type: 'Event', color: 'red', cost: 1, level: 2 }
      }
      text '[ACT][(1)] Brainstorm 4, put a red character from the waiting room into your hand'
    end

    trait :climax do
      name 'Magical Mystery Cure'
      first_printing_number 'MP/S42-E101'
      printings {
        [
          Printing.new( number: 'MP/S42-E101', rarity: 'CC')
        ]
      }
      card_trigger { { come_back: 1, soul: 1 } }
      expansion { { name: 'My Little Pony' } }
      metadatum {
        { card_type: 'Climax', color: 'blue' }
      }
      text '+1000 power and +1 soul to all your characters'
    end
  end
end
