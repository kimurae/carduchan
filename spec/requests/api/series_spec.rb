require 'rails_helper'

RSpec.describe '/api/series' do

  describe 'GET /api/series' do
    before do
      FactoryGirl.create(:series)
      get '/api/series.json'
    end

    specify do
      expect(response).to have_http_status(:ok)
      expect(response).to match_response_schema('api/series/index')
    end
  end
end
