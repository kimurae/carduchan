require 'rails_helper'

RSpec.describe '/api/series/:series_id/cards' do

  describe 'GET /api/series/:series_id/cards' do
    let(:series) { FactoryGirl.create(:series) }

    before do
      FactoryGirl.create(:card, :character, series_id: series.id, image_url: '/')

      get "/api/series/#{series.id}/cards.json"
    end

    specify do
      expect(response).to have_http_status(:ok)
      expect(response).to match_response_schema('api/series/cards/index')
    end
  end
end
