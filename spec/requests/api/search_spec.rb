require 'rails_helper'

RSpec.describe '/api/search/cards.json' do

  describe 'GET /api/search/cards.json' do

    before do
      FactoryGirl.create(:card, :character, name: 'Loyal Friend, Rainbow Dash')
      ActiveRecord::Base.connection.execute('REFRESH MATERIALIZED VIEW documents;')
    end

    before do
      get "/api/search.json", params: { name: 'rainbow' }
    end

    specify do
      expect(response).to have_http_status(:ok)
      expect(response).to match_response_schema('api/search/index')
    end
  end
end
