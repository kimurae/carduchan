require 'rails_helper'

RSpec.describe '/api/card_facets' do

  describe 'GET /api/card_facets' do
    before do
      FactoryGirl.create(:card, :character)
      get '/api/card_facets.json'
    end

    specify do
      expect(response).to have_http_status(:ok)
      expect(response).to match_response_schema('api/card_facets/index')
    end
  end
end
