require 'rails_helper'

RSpec.describe Printing, type: :model do

  let(:card) { Card.create!(name: 'test', text: 'test') }

  it 'should have defaults' do
    expect(Printing.new.rarity).to eq 'Unknown'
  end

  describe '#save' do

    context 'with valid attributes' do
      it 'should create one record' do
        expect {
          Printing.new(number: 'MP/S42-E109', card: card ).save
        }.to change(Printing, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'should not create a record' do
        expect {
          Printing.new(number: nil).save
        }.to change(Printing, :count).by(0)
      end
    end
  end

  describe '#valid?' do

    context 'number' do

      it 'should not be valid if nil' do
        expect(Printing.new(number: nil, card: card)).to_not be_valid
      end

      it 'must be unique' do
        Printing.create(number: 'MP/S42-E109', card: card)
        expect(Printing.new(number: 'MP/S42-E109', card: card)).to_not  be_valid
        expect(Printing.new(number: 'MP/S42-E042', card: card)).to      be_valid
      end
    end

    context 'rarity' do

      it 'should not be valid if nil' do
        expect(Printing.new(number: 'MP/S42-E109', rarity: nil, card: card)).to_not be_valid
      end

      it 'should not be valid if not SP, SR, RRR, RR, R, U, C, CR, CC, TD, PR, SSP, XR, RR+, WR, GR or Unknown' do
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'My Little Pony', card: card)).to_not be_valid
      end

      it 'should be valid if SP, SR, RRR, RR, R, U, C, CR, CC, TD, PR, SSP, XR, RR+, WR, GR or Unknown' do
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'SP', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'SR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'RRR', card: card)).to  be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'RR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'R', card: card)).to    be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'U', card: card)).to    be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'C', card: card)).to    be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'CR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'CC', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'TD', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'PR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'SSP', card: card)).to  be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'XR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'RR+', card: card)).to  be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'WR', card: card)).to   be_valid
        expect(Printing.new(number: 'MP/S42-E109', rarity: 'GR', card: card)).to   be_valid
      end
    end
  end

end
