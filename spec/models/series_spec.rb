require 'rails_helper'

RSpec.describe Series, type: :model do

  describe '#save' do

    it 'should save and one record' do
      expect {
        Series.new(name: 'My Little Pony').save
      }.to change(Series, :count).by(1)
    end

  end
end
