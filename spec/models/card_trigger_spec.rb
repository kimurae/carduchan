require 'rails_helper'

RSpec.describe CardTrigger, type: :model do

  it 'should have defaults' do
    expect(CardTrigger.new.come_back).to  be_zero
    expect(CardTrigger.new.draw).to       be_zero
    expect(CardTrigger.new.gate).to       be_zero
    expect(CardTrigger.new.return).to     be_zero
    expect(CardTrigger.new.shot).to       be_zero
    expect(CardTrigger.new.treasure).to   be_zero
  end

  describe '#save' do

    context 'with valid attributes' do
      it 'should create one record' do
        expect {
          CardTrigger.new(come_back: 2).save
        }.to change(CardTrigger, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'should not create a record' do
        expect {
          CardTrigger.new(come_back: nil).save
        }.to change(CardTrigger, :count).by(0)
      end
    end
  end

  describe '#valid?' do

    context 'come_back' do

      it 'should not be valid if nil' do
        expect(CardTrigger.new(come_back: nil)).to_not be_valid
      end
    end

    context 'draw' do
      it 'should not be valid if nil' do
        expect(CardTrigger.new(draw: nil)).to_not be_valid
      end
    end

    context 'gate' do
      it 'should not be valid if nil' do
        expect(CardTrigger.new(gate: nil)).to_not be_valid
      end
    end

    context 'return' do
      it 'should not be valid if nil' do
        expect(CardTrigger.new(return: nil)).to_not be_valid
      end
    end

    context 'shot' do
      it 'should not be valid if nil' do
        expect(CardTrigger.new(shot: nil)).to_not be_valid
      end
    end

    context 'treasure' do
      it 'should not be valid if nil' do
        expect(CardTrigger.new(treasure: nil)).to_not be_valid
      end
    end

  end

end
