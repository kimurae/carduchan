require 'rails_helper'

RSpec.describe Card, type: :model do

  it 'should have defaults' do
    expect(Card.new.name).to eq 'Unknown'
  end

  context 'a valid card' do

    let(:valid_attributes) do
      {
        series_id: 42,
        image_url: 'dummy.jpg',
        name: 'Town Leader, Starlight Glimmer',
        text: '[AUTO] All other characters lose all printed abilities.',
        traits: %w(unicorn magic)
      }
    end

    describe '#save' do

      it 'should save a valid card' do
        expect {
          Card.new(valid_attributes).save
        }.to change(Card, :count).by(1)
      end
    end
  end

  context 'an invalid card' do

    describe '#save' do

      it 'should not save an invalid card' do
        expect {
          Card.new(name: nil).save
        }.to change(Card, :count).by(0)
      end
    end
  end

  describe '#card_trigger=' do
    subject { Card.new }

    context 'with a hash' do
      let(:card_trigger) { FactoryGirl.build(:card_trigger, soul: 2) }

      before do
        allow(CardTrigger).to receive(:find_or_create_by!).and_return(card_trigger)
      end

      it 'should find or create a new card_trigger with the conditions' do
        subject.card_trigger = { soul: 2 }
        expect(subject.card_trigger).to eq(card_trigger)
      end
    end

    context 'with a CardTrigger' do
      let(:card_trigger) { FactoryGirl.build(:card_trigger) }

      it 'should act per normal' do
        subject.card_trigger = card_trigger
        expect(card_trigger).to eq(card_trigger)
      end

    end
  end

  describe '#metadatum=' do
    subject { Card.new }

    context 'with a hash' do
      let(:metadatum) { FactoryGirl.build(:metadatum, soul: 2) }

      before do
        allow(Metadatum).to receive(:find_or_create_by!).and_return(metadatum)
      end

      it 'should find or create a new metadatum with the conditions' do
        subject.metadatum = { soul: 2 }
        expect(subject.metadatum).to eq(metadatum)
      end
    end

    context 'with a Metadatum' do
      let(:metadatum) { FactoryGirl.build(:metadatum) }

      it 'should act per normal' do
        subject.metadatum = metadatum
        expect(metadatum).to eq(metadatum)
      end

    end
  end

  describe '#expansion=' do
    subject { Card.new }

    context 'with a hash' do
      let(:expansion) { FactoryGirl.build(:expansion, name: 'My Little Pony') }

      before do
        allow(Expansion).to receive(:find_or_create_by!).and_return(expansion)
      end

      it 'should find or create a new expansion with the conditions' do
        subject.expansion = { name: 'My Little Pony' }
        expect(subject.expansion).to eq(expansion)
      end
    end

    context 'with a Expansion' do
      let(:expansion) { FactoryGirl.build(:expansion) }

      it 'should act per normal' do
        subject.expansion = expansion
        expect(expansion).to eq(expansion)
      end

    end
  end

end
