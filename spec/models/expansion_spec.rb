require 'rails_helper'

RSpec.describe Expansion, type: :model do
  it 'should initialize with defaults' do
    expect(Expansion.new.name).to eq 'Unknown'
  end

  describe '#save' do
    it 'with valid attributes it should save' do
      expect {
        Expansion.new(name: 'My Little Pony').save
      }.to change(Expansion, :count).by(1)
    end
  end

  describe '#valid?' do

    it 'should have unique rows' do
      Expansion.create!(name: 'My Little Pony')

      expect(Expansion.new(name: 'My Little Pony')).to_not be_valid
      expect(Expansion.new(name: 'Rainbow Brite')).to be_valid
    end

    context '#name' do

      it 'should be invalid if name is empty' do
        expect(Expansion.new(name: nil)).to_not be_valid
      end
    end
  end

end
