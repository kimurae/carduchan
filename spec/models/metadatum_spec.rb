require 'rails_helper'

RSpec.describe Metadatum, type: :model do

  it 'should initialize with defaults' do
    expect(Metadatum.new.card_type).to eq 'Unknown'
    expect(Metadatum.new.color).to     eq 'unknown'
    expect(Metadatum.new.cost).to      be_zero
    expect(Metadatum.new.level).to     be_zero
    expect(Metadatum.new.power).to     be_zero
    expect(Metadatum.new.soul).to      be_zero
  end

  describe '#save' do
    it 'with valid attributes it should save' do
      expect {
        Metadatum.new(card_type: 'Character', color: 'green').save
      }.to change(Metadatum, :count).by(1)
    end
  end

  describe '#valid?' do

    it 'should have unique rows' do
      Metadatum.create!(card_type: 'Character', color: 'green')

      expect(Metadatum.new(card_type: 'Character', color: 'green')).to_not be_valid
      expect(Metadatum.new(card_type: 'Character', color: 'yellow')).to be_valid
    end

    context '#card_type' do

      it 'should be valid if card_type is Character, Climax, Event or Unknown' do
        expect(Metadatum.new(card_type: 'Character')).to be_valid
        expect(Metadatum.new(card_type: 'Climax')).to be_valid
        expect(Metadatum.new(card_type: 'Event')).to be_valid
        expect(Metadatum.new(card_type: 'Unknown')).to be_valid
      end

      it 'should be invalid if card_type is not Character, Climax, Event or Unknown' do
        expect(Metadatum.new(card_type: 'Pony')).to_not be_valid
      end

      it 'should be invalid if card_type is empty' do
        expect(Metadatum.new(card_type: nil)).to_not be_valid
      end
    end

    context '#color' do

      it 'should be valid if color is blue,green,red, yellow, or unknown' do
        expect(Metadatum.new(color: 'blue')).to be_valid
        expect(Metadatum.new(color: 'green')).to be_valid
        expect(Metadatum.new(color: 'red')).to be_valid
        expect(Metadatum.new(color: 'yellow')).to be_valid
        expect(Metadatum.new(color: 'unknown')).to be_valid
      end

      it 'should be invalid if color is not blue, green, red, yellow or unknown' do
        expect(Metadatum.new(color: 'pink')).to_not be_valid
      end

      it 'should be invalid if color is empty' do
        expect(Metadatum.new(color: nil)).to_not be_valid
      end
    end

    context '#cost' do

      it 'should be valid if cost is set to an integer' do
        expect(Metadatum.new(cost: '3')).to be_valid
        expect(Metadatum.new(cost: 3)).to be_valid
      end

      it 'should be invalid if cost is set to a decimal' do
        expect(Metadatum.new(cost: '3.2')).to_not be_valid
        expect(Metadatum.new(cost: 3.2)).to_not be_valid
      end

      it 'should be invalid if nil is passed to cost' do
        expect(Metadatum.new(cost: nil)).to_not be_valid
      end

      it 'should be invalid if set to a string' do
        expect(Metadatum.new(cost: 'cat')).to_not be_valid
      end
    end

    context '#level' do

      it 'should be valid if level is 0,1,2 or 3' do
        expect(Metadatum.new(level: '0')).to be_valid
        expect(Metadatum.new(level: 0)).to be_valid
        expect(Metadatum.new(level: '1')).to be_valid
        expect(Metadatum.new(level: 1)).to be_valid
        expect(Metadatum.new(level: '2')).to be_valid
        expect(Metadatum.new(level: 2)).to be_valid
        expect(Metadatum.new(level: '3')).to be_valid
        expect(Metadatum.new(level: 3)).to be_valid
      end

      it 'should not be valid if level is below zero or above three' do
        expect(Metadatum.new(level: '-1')).to_not be_valid
        expect(Metadatum.new(level: -1)).to_not be_valid
        expect(Metadatum.new(level: '4')).to_not be_valid
        expect(Metadatum.new(level: 4)).to_not be_valid
      end

      it 'should not be valid if nil is passed to level' do
        expect(Metadatum.new( color: 'green', level: nil)).to_not be_valid
      end

      it 'should not be valid if level is set to a decimal' do
        expect(Metadatum.new(level: '1.2')).to_not be_valid
        expect(Metadatum.new(level: 1.2)).to_not be_valid
      end

      it 'should not be valid if level is set to a string' do
        expect(Metadatum.new(level: 'pink')).to_not be_valid
      end
    end

    context '#soul' do

      it 'should be valid if soul is set to an integer' do
        expect(Metadatum.new(soul: '3')).to be_valid
        expect(Metadatum.new(soul: 3)).to be_valid
      end

      it 'should be invalid if nil is passed to soul' do
        expect(Metadatum.new(soul: nil)).to_not be_valid
      end

      it 'should be invalid if set to a string' do
        expect(Metadatum.new(soul: 'cat')).to_not be_valid
      end
    end

    context '#power' do

      it 'should be valid if power is an integer' do
        expect(Metadatum.new(power: '1200')).to be_valid
        expect(Metadatum.new(power: 1200)).to be_valid
      end

      it 'should not be valid if power is set to nil' do
        expect(Metadatum.new(power: nil)).to_not be_valid
      end

      it 'should not be valid if power is set to a string' do
        expect(Metadatum.new(power: 'over 9000!')).to_not be_valid
      end

    end
  end
end
