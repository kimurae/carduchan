require 'rails_helper'

RSpec.describe 'The root site', js: true do

  it 'should render a default welcome page' do
    visit '/'
    expect(page).to have_content 'Series'
  end
end
