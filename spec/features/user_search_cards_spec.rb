require 'rails_helper'

RSpec.feature 'User is able to search cards', js: true do

  scenario 'User types in search bar and views expected results' do
    FactoryGirl.create(:card, :character)
    ActiveRecord::Base.connection.execute('REFRESH MATERIALIZED VIEW documents;')

    visit '/'

    fill_in :card_text, with: 'Starlight'

    find('.btn').click

    expect(page).to have_content 'Search Results'
    expect(page).to have_content 'Town Leader, Starlight Glimmer'
  end

  scenario 'User fills in a search form and views expected results' do
    FactoryGirl.create(:card, :character)

    visit '/'
    click_link 'Advanced Search'

    find('#card-type-character').set(true)
    find('#card-color-yellow').set(true)
    find('#card-cost-2').set(true)
    find('#card-level-3').set(true)
    find('#card-power-10000').set(true)
    find('#card-soul-3').set(true)
    find('#card-rarity-u').set(true)
    find('#expansion-my-little-pony').set(true)
    find('#card-trait-unicorn').set(true)
    find('#card-trait-magic').set(true)

    click_button 'Submit'

    expect(page).to have_content 'Search Results'
    expect(page).to have_content 'Town Leader, Starlight Glimmer'
  end
end
