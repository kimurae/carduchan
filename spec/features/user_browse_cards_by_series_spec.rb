require 'rails_helper'

RSpec.feature 'User can browse cards by series', js: true do

  scenario 'User is able to view the My Little Pony cards' do

    Series.create!(name: 'My Little Pony').tap do |s|

      FactoryGirl.create(:card, :character, series_id: s.id, first_printing_number: 'MP/S42-E103')
      FactoryGirl.create(:card, :event, series_id: s.id, first_printing_number: 'MP/S42-E104')
      FactoryGirl.create(:card, :climax, series_id: s.id, first_printing_number: 'MP/S42-E105')
    end

    visit '/'
    click_link 'Browse'
    click_link 'My Little Pony'

    within('.page-header') do
      expect(page).to have_content 'My Little Pony'
    end

    within('#card-mp-s42-e103') do
      within('span.yellow') do
        expect(page).to have_content 'Town Leader, Starlight Glimmer'
      end

      expect(page).to have_content 'Character'
      expect(page).to have_content 'My Little Pony'

      expect(page).to have_content '32'

      expect(page).to have_content 'MP/S42-E103'
      expect(page).to have_content 'MP/S42-TE01'
      expect(page).to have_content 'U'
      expect(page).to have_content 'TD'
      expect(page).to have_content '10000'
      expect(page).to have_content 'AUTO'
      expect(page).to have_content 'All other characters lose all printed abilities.'
      expect(page).to have_content 'Unicorn'
      expect(page).to have_content 'Magic'
      expect(page).to have_content 'TRIGGER SOUL'
    end

    within('#card-mp-s42-e104') do
      within('span.red') do
        expect(page).to have_content 'Crystal Faire'
      end

      expect(page).to have_content 'Event'
      expect(page).to have_content 'My Little Pony'

      expect(page).to have_content '21'

      expect(page).to have_content 'MP/S42-E102'
      expect(page).to have_content 'RRR'
      expect(page).to have_content 'ACT'
      expect(page).to have_content '1'
      expect(page).to have_content 'Brainstorm 4, put a red character from the waiting room into your hand'
    end

    within('#card-mp-s42-e105') do
      within('span.blue') do
        expect(page).to have_content 'Magical Mystery Cure'
      end

      expect(page).to have_content 'Climax'
      expect(page).to have_content 'My Little Pony'

      expect(page).to have_content 'MP/S42-E101'
      expect(page).to have_content 'CC'
      expect(page).to have_content '+1000 power and +1 soul to all your characters'
      expect(page).to have_content 'TRIGGER COME BACK'
      expect(page).to have_content 'TRIGGER SOUL'
    end
  end
end
