describe('InlineCheckboxGroup', function() {
  'use strict';

  var Utils = React.addons.TestUtils;

  var element = React.createElement( InlineCheckboxGroup,{
    id: 'search-card-type',
    label: 'Card Type',
    name: 'card_type',
    data: [
      { value: 'Character', label: 'Character', sanitized_value: 'character' },
      { value: 'Event',     label: 'Event',     sanitized_value: 'event' }
    ]
  });

  it('can render without error', function() {
    var component;

    console.log(this);
    expect(function() { component = Utils.renderIntoDocument(element); }).not.toThrow();
  });

  it('renders the correct amount of inputs', function() {

    expect(React.findDOMNode( Utils.renderIntoDocument(element)).querySelectorAll('label input').length).toBe(2);

  });

})
