describe('CardFacetSearchComponent', function() {
  var Utils = React.addons.TestUtils;

  it('can render without error', function() {
    var component, element;

    element = React.createElement( CardFacetsSearchComponent,{});

    expect(function() { component = Utils.renderIntoDocument(element); }).not.toThrow();
  });

})
