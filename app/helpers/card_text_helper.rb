module CardTextHelper
  def make_pretty(text)
    h(text).
      gsub(/.(AUTO|ACT|CONT)./, '<span class="label label-primary">\1</span>').
      gsub(/.(COUNTER)./, '<span class="label label-danger">\1</span>').
      gsub(/\((\d)\)/,'<span class="badge">\1</span>').
      gsub(/\((.*?)\)/,'<emph>(\1)</emph>').
      split(/\n/).join('</p><p>').
      html_safe
  end
end
