# Generates a subquery to perform a Postgres text search on a model.
# @see [Document]
# @see [SearchQuery]
class TextSearchQuery

  # @param column [Symbol] tsvector column to search.
  # @param relation [ActiveRelation] owner of the column.
  def initialize(column:, relation:)
    @column   = relation.arel_table[column]
    @relation = relation
  end

  # Generates the query to search a tsvector, and orders by rank (in the tsvector).
  # @param text [String] the search criteria.
  # @return [ActiveRelation] the text search query.
  def call(text)
    relation.
    where( Arel::Nodes::InfixOperation.new('@@', column, tsquery(text)) ).
    order( Arel::Nodes::NamedFunction.new('ts_rank', [column, tsquery(text)]).desc )
  end

  private

  include ActiveRecord::Sanitization::ClassMethods

  attr_reader :column, :relation

  def tsquery(text)
    Arel::Nodes::NamedFunction.new('plainto_tsquery',
      [ Arel::Nodes.build_quoted('english'),
        Arel::Nodes.build_quoted(sanitize_sql_like(text))
      ]
    )
  end
end
