# Generates the search query for Basic and Advanced Search.
class SearchQuery

  private_class_method :new

  class << self
    delegate :call, to: :new
  end

  # Generates a query to search via criteria provided.
  # It tries to only join portions based off what criteria are present.
  # This keeps most criteria optional.
  # @param query [Hash] criteria for the search query.
  # @return [ActiveRelation] the query to execute the search.
  def call(query)
    [
      relation.with_kitchen_sink,
      search(query[:card_text]),
      with_document(query[:card_text]),
      with_expansions(query[:card_expansion]),
      with_metadata(query[:metadata].try(:compact)),
      with_rarities(query[:card_rarity]),
      with_traits(query[:card_trait])
    ].compact.reduce(&:merge)
  end

  private

  def relation
    Card
  end

  # After manual testing it appears as though at least.... postgres does not
  # alllow multiple statments via order clause... so ; DROP table... won't work
  # here. Wish I could use ? in ORDER BY.
  def search(text)
    if text.present?
      Document.search(text)
    else
      relation.order(:first_printing_number)
    end
  end

  def with_document(text)
    if text.present?
      relation.joins(:document)
    end
  end

  def with_expansions(expansion_id)
    if expansion_id.present?
      relation.where(expansion_id: expansion_id)
    end
  end

  def with_metadata(query)
    if query.present?
      relation.where(metadatum_id: Metadatum.where(query).select(:id))
    end
  end

  def with_rarities(rarities)
    if rarities.present?
      relation.where(id: Printing.where(rarity: rarities).select(:card_id))
    end
  end

  def with_traits(traits)
    if traits.present?
      relation.with_traits(traits)
    end
  end

end
