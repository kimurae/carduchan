# A Printing of a card. (Cards can have multiple printings, a foil or signed version or a reprint for example)
class Printing < ApplicationRecord
  extend DimensionalParentConcern

  belongs_to :card

  validates :number, presence: true, uniqueness: true
  validates :rarity, inclusion: { in: %w(SP SR RRR RR R U C CR CC TD PR SSP XR RR+ WR GR Unknown) }

  # This provides the rarity checkboxes on the Advanced Search form.
  # @return [Array<String>] a list of every possible rarity.
  def self.all_distinct_rarities
    pluck('DISTINCT rarity')
  end
end
