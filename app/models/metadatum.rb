# A dimensional table for a card's metadata.
class Metadatum < ApplicationRecord
  has_many :cards, dependent: :restrict_with_exception

  validates :card_type, inclusion: { in: %w(Character Climax Event Unknown) },
                        uniqueness: { scope: %i(color cost level power soul), message: 'Metadata must be unique' }

  validates :color,     inclusion: { in: %w(blue green red yellow unknown) }
  validates :cost,      presence: true, numericality: { only_integer: true }
  validates :level,     numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }
  validates :power,     presence: true, numericality: { only_intger: true }
  validates :soul,      numericality: { only_intger: true }

  # This provides the checkboxes on the Advanced Search Page.
  # @return Hash permutations of all possible values of each metatatum.
  def self.facets
    {
      card_types: %w(Character Climax Event),
      colors:     %w(blue green red yellow),
      costs:      order(:cost).pluck('DISTINCT cost'),
      levels:     [0,1,2,3],
      powers:     order(:power).pluck('DISTINCT power'),
      souls:      order(:soul).pluck('DISTINCT soul')
    }
  end
end
