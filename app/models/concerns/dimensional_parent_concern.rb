# For models that have a dimensional parent.
module DimensionalParentConcern

  # Ensures that when a child is initialized, it has an initial parent
  # record.
  # It also provides a setter method that accepts either an instance of
  # the parent or a hash of parental data used to find or create
  # an existing parent.
  # @param reflection_name [Symbol] the name of the parent association.
  def dimensional_parent(reflection_name)

    klass = reflections[reflection_name.to_s].klass

    setter_method_name = :"#{reflection_name}="

    define_method setter_method_name do |new_value|

      super(
        if new_value.kind_of?(klass)
          new_value
        else
          klass.find_or_create_by!(new_value.compact)
        end )
    end

    after_initialize Proc.new {
      if new_record? && send(reflection_name.to_sym).nil?
        send(setter_method_name, klass.new)
      end
    }

  end

end
