# For models pointing to views.
module ModelIsAViewConcern

  # * Sets the model to readonly, as its a view.
  # * Sets the equality comparison methods to use the list of keys provided (instead of id).
  # @param keys [Array<Symbol>] a list of keys used to determine record equality.
  def is_a_view(keys:)

    define_method :eql? do |result|
      keys.all? { |key| send(key) == result.public_send(key) }
    end

    define_method :hash do
      keys.map { |key| send(key) }.hash
    end

    define_method :readonly? do
      true
    end
  end
end
