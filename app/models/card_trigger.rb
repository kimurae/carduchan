# A specific combination of card triggers.
# Card triggers are mechanical effects that trigger when a card
# is revealed when acquring 'stock'. (See the Weiss Schwartz rules)
class CardTrigger < ApplicationRecord
  has_many :cards

  validates :come_back, presence: true, numericality: true
  validates :draw,      presence: true, numericality: true
  validates :gate,      presence: true, numericality: true
  validates :return,    presence: true, numericality: true
  validates :shot,      presence: true, numericality: true
  validates :soul,      presence: true, numericality: true
  validates :treasure,  presence: true, numericality: true

end
