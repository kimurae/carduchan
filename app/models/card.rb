# A Weiss Schwartz Card
class Card < ApplicationRecord
  extend DimensionalParentConcern

  belongs_to :card_trigger
  belongs_to :expansion
  belongs_to :metadatum
  belongs_to :series

  has_one    :document

  has_many   :printings

  validates :name, presence: true, uniqueness: { scope: %i(expansion_id series_id text) }

  dimensional_parent :card_trigger
  dimensional_parent :expansion
  dimensional_parent :metadatum
  dimensional_parent :series

  delegate :come_back,
    :draw,
    :gate,
    :pool,
    :return,
    :shot,
    :soul,
    :treasure,
    to: :card_trigger, prefix: :trigger

  delegate :name, to: :expansion, prefix: true

  delegate :card_type,
          :color,
          :cost,
          :level,
          :power,
          :power?,
          :soul,
          to: :metadatum

  scope :ordered, ->() {
    order(:first_printing_number)
  }

  scope :search, SearchQuery

  # Preloads CardTrigger, Expansion, Metadatum, Printings
  scope :with_kitchen_sink, ->() {
    includes(:card_trigger).
    includes(:expansion).
    includes(:metadatum).
    includes(:printings)
  }

  scope :with_traits, ->(traits) {
    where( Arel::Nodes::InfixOperation.new('@>', arel_table[:traits], Arel.sql('ARRAY[?]::varchar[]')).to_sql, traits)
  }

  # @return [Array<string>] List of each unique card trait in the database.
  def self.all_distinct_traits
    pluck('DISTINCT UNNEST(traits)')
  end

  # @return [Array<String>] List of triggers this card has.
  def triggers
    %i(trigger_come_back trigger_draw trigger_gate trigger_pool trigger_return trigger_shot trigger_soul trigger_treasure).flat_map do |meth|
      send(meth).times.map { meth.to_s }
    end.compact
  end
end
