# A card expansion, in weiss schwartz cards are grouped into expansion sets,
# when released.
class Expansion < ApplicationRecord
  has_many :cards, dependent: :restrict_with_exception

  validates :name, presence: true, uniqueness: true
end
