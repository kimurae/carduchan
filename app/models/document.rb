# A Materialized View of the text search data for cards.
class Document < ActiveRecord::Base
  extend ModelIsAViewConcern

  belongs_to :card

  is_a_view keys: :card_id

  scope :search, TextSearchQuery.new(relation: self, column: :document)
end
