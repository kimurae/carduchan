class SeriesController < ApplicationController

  helper_method :series

  def index; end

  private

  def series
    @series ||= Series.all
  end

end
