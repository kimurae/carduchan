class SearchController < ApplicationController

  helper_method :search_results

  def new
    render layout: false
  end

  def create
    render layout: false
  end

  private

  def search_params
    params.permit(
      :card_text,
      card_expansion: [],
      card_rarity: [],
      card_trait: [],
      metadata: { card_type: [], color: [], cost: [], level: [], power: [], soul: []},
    ).to_h
  end

  def search_results
    @search_results ||= Card.search(search_params)
  end

end
