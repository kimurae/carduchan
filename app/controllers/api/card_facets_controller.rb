module Api
  class CardFacetsController < Base
    helper_method :card_facets

    def index; end

    private

    def card_facets
      @card_facets ||= {
        card_types: metadata[:card_types],
        colors:     metadata[:colors],
        costs:      metadata[:costs],
        expansions: expansions,
        levels:     metadata[:levels],
        powers:     metadata[:powers],
        rarities:   rarities,
        souls:      metadata[:souls],
        traits:     traits
      }
    end

    def metadata
      @metadata ||= Metadatum.facets
    end

    def expansions
      Expansion.all.order(:name)
    end

    def rarities
      Printing.all_distinct_rarities.sort
    end

    def traits
      Card.all_distinct_traits.sort
    end

  end
end
