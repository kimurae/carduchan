module Api
  class CardsController < Base
    include ActionController::Helpers

    helper CardTextHelper

    helper_method :cards, :image_for, :series_name

    def index; end

    private

    delegate :name, to: :series, prefix: true

    def cards
      @cards ||= series.cards.ordered.with_kitchen_sink
    end

    def image_for(card)
      'dummy.jpg'
    end

    def series
      @series ||= Series.find_by_id(params[:series_id])
    end

  end
end
