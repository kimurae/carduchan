module Api
  class SearchController < CardsController

    private

    def cards
      @cards ||= Card.search(search_params)
    end

    def search_params
      params.permit(
        :card_text,
        card_expansion: [],
        card_rarity: [],
        card_trait: [],
        metadata: { card_type: [], color: [], cost: [], level: [], power: [], soul: []},
      ).to_h
    end

  end
end
