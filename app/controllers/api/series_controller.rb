module Api
  class SeriesController < Base

    helper_method :series

    def index; end

    private

    def series
      Series.all
    end
  end
end
