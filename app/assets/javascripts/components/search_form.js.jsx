var SearchForm = React.createClass({
  render: function() {
    return(
      <form id="card-search-form" method="POST" action="/search" role="search">
        <input type="hidden" name="authenticity_token" value={this.props.authenticity_token} />
        <fieldset>
          <div className="form-group">
            <label for="card-text">Card Name, Card Text, Expansion or Card Number.</label>
            <input className="form-control" id="card-text" type="text" name="card_text" />
          </div>
        </fieldset>
        <CardFacetsSearchComponent />
        <fieldset>
          <button type="submit" className="btn btn-primary"><span className="glyphicon glyphicon-search" aria-hidden="true"></span> Submit</button>
        </fieldset>
      </form>
    )
  }
});
