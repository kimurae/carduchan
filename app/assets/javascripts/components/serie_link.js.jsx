var SerieLink = React.createClass({

  handleClick: function(e) {
    e.preventDefault();
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.props.onDataLoad(data.cards, this.props.name, 'Card');
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  render: function() {
    return(
      <article className="serie">
        <a href='#' onClick={this.handleClick} >{this.props.name}</a>
      </article>
    );
  }
});
