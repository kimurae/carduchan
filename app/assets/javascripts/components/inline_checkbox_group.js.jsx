var InlineCheckboxGroup = React.createClass({
  checkboxComponents: function() {
    return this.props.data.map(function(row) {
      return (
        <label key={"label-" + row.sanitized_value} className="checkbox-inline">
          <input key={"checkbox-" + row.sanitized_value} id={this.props.id + "-" + row.sanitized_value} type="checkbox" name={this.props.name} value={row.value} /> {row.label}
        </label>
      );
    }, this);
  },
  render: function() {
    return (
      <div className="form-group" id={this.props.id}>
        <fieldset>
          <legend>{this.props.label}</legend>
          {this.checkboxComponents()}
        </fieldset>
      </div>
    );
  }
});
