var SeriesList = React.createClass({
  getInitialState: function() {
    return {};
  },
  render: function() {
    return(
     <main>
      { this.props.data.map(function(record) {
          switch(this.props.type) {
            case 'Series':
              return ( <SerieLink key={record.react_id} onDataLoad={this.props.onDataLoad} {...record} /> );
              break;
            case 'Card':
              return ( <Card key={record.react_id} {...record} /> );
              break;
            default:
              return null;
          }
        }, this)}
     </main>
    )
  }
});

// Have it such that it accepts external events to re-render it with cards?
// or maybe just have an onclick handler here that fetches and replaces data with the cards.
