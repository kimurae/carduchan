var CardFacetsSearchComponent = React.createClass({
  componentDidMount: function() {
    $.ajax({
      url: '/api/card_facets.json',
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({
          card_types: data.card_facets.card_types,
          colors:     data.card_facets.colors,
          costs:      data.card_facets.costs,
          expansions: data.card_facets.expansions,
          levels:     data.card_facets.levels,
          powers:     data.card_facets.powers,
          rarities:   data.card_facets.rarities,
          souls:      data.card_facets.souls,
          traits:     data.card_facets.traits
        });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {
      card_types: [],
      colors:     [],
      costs:      [],
      expansions: [],
      levels:     [],
      powers:     [],
      rarities:   [],
      souls:      [],
      traits:     []
    };
  },
  render: function() {
    return (
      <fieldset className="card-facets-search-component">
        <div className="row">
          <div className="col-xs-12 col-md-4">
            <InlineCheckboxGroup id="card-type"      name="metadata[card_type][]" label="Card Type"      data={this.state.card_types} />
          </div>
          <div className="col-xs-12 col-md-4">
            <InlineCheckboxGroup id="card-color"     name="metadata[color][]"     label="Card Color"     data={this.state.colors} />
          </div>
          <div className="col-xs-12 col-md-4">
            <InlineCheckboxGroup id="card-cost"      name="metadata[cost][]"      label="Card Cost"      data={this.state.costs} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <ColumnedCheckboxGroup id="expansion"      name="card_expansion[]"      label="Card Expansion" data={this.state.expansions} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 col-md-4">
            <InlineCheckboxGroup id="card-level"     name="metadata[level][]"     label="Card Level"     data={this.state.levels} />
          </div>
          <div className="col-xs-12 col-md-4">
            <ColumnedCheckboxGroup id="card-power"     name="metadata[power][]"     label="Card Power"     data={this.state.powers} />
          </div>
          <div className="col-xs-12 col-md-4">
            <ColumnedCheckboxGroup id="card-rarity"    name="card_rarity[]"         label="Card Rarity"    data={this.state.rarities} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 col-md-4">
            <InlineCheckboxGroup id="card-soul"      name="metadata[soul][]"      label="Card Soul"      data={this.state.souls} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <ColumnedCheckboxGroup id="card-trait"     name="card_trait[]"          label="Card Traits"    data={this.state.traits} />
          </div>
        </div>
      </fieldset>
    )
  }
});
