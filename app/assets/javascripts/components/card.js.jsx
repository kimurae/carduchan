var Card = React.createClass({

  render: function() {

    return (
      <article className={this.props.card_type.toLowerCase()} id={this.props.react_id}>
        <section className="col-md-2 card-image">
          <div className="row"><div className="col-xs-12">
            <img src={this.props.image_url} width="170"/>
          </div></div>
        </section>
        <section className="col-md-10">
          <div className="row"><div className="col-xs-12">
            <span className={'card-title ' + this.props.color}>{this.props.name}</span>
          </div></div>
          <div className="row">
          <section className="col-sm-12 col-md-8">
            <section className="row">
              <ol className="card-metadata">
                <li className="card-type">{this.props.card_type}</li>
                { this.props.traits.map(function(trait) {
                    return ( <li className="card-trait" key={trait.react_id}>{trait.trait}</li> );
                  }, this)}
                <li className="card-level">{this.props.level}</li>
                <li className="card-cost">{this.props.cost}</li>
                <li className="card-power"><strong>Power:</strong> {this.props.power}</li>
                <li className="card-soul"><strong>Soul:</strong> {this.props.soul}</li>
              </ol>
              <ul className="card-triggers">
                { this.props.triggers.map(function(trigger) {
                    return ( <li className="card-trigger" key={trigger.react_id}><span className="label label-info">{trigger.trigger}</span></li> );
                  }, this)}
              </ul>
            </section>
            <p className="card-text" dangerouslySetInnerHTML={{__html: this.props.text }} />
          </section>
          <section className="col-sm-12 col-md-2">
            <strong>{this.props.expansion_name}</strong>
            <ul className="card-printings">
              { this.props.printings.map(function(printing) {
                  return ( <li key={printing.react_id}><span className="badge">{printing.rarity}</span> {printing.number}</li> );
                }, this)}
            </ul>
          </section>
          </div>
        </section>
      </article>
    );
  }
});
