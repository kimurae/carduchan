class CarduchanApp extends React.Component {
  constructor() {
    super();
    this.state = { data: [], header: '', type: 'Unknown' };

    this.handleDataLoad = (data, header, type) => this.setState({data: data, header: header, type: type });

    this.handleBrowseClick = (e) => {
      e.preventDefault();
      this._loadSeries.apply(this);
    }
  }
  componentDidMount () {
    this._loadSeries.apply(this);
  }
  _loadSeries () {
    $.ajax({
      url: '/api/series.json',
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({ data: data.series, header: 'Series', type: 'Series' });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error('/api/series.json', status, err.toString());
      }.bind(this)
    });
  }
  render () {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-6 col-md-offset-3 text-center">
            <SearchBar authenticity_token={this.props.authenticityToken} onDataLoad={this.handleDataLoad} />
            <p>
              <a href="#" onClick={this.handleBrowseClick}>Browse</a> | <a href="/search/new">Advanced Search</a>
            </p>
          </div>
        </div>
        <div className="page-header">
          <h2>{this.state.header}</h2>
        </div>
        <div className="row">
          <SeriesList onDataLoad={this.handleDataLoad} data={this.state.data} type={this.state.type} />
        </div>
      </div>
    );
  }
}

CarduchanApp.propTypes = {
  authenticityToken: React.PropTypes.string
};
