var SearchBar = React.createClass({
  getInitialState: function() {
    return { cardText: '' };
  },
  handleSubmit: function(e) {
    if(this.props.onDataLoad == undefined) {
      return true;
    }
    e.preventDefault();
    $.ajax({
      url: '/api/search.json',
      dataType: 'json',
      cache: false,
      type: 'GET',
      data: {
        card_text: this.state.cardText
      },
      success: function(data) {
        this.props.onDataLoad(data.cards, 'Search Results', 'Card');
      }.bind(this),
      error: function(xhr, status, err) {
        console.error('GET /api/search.json', status, err.toString());
      }.bind(this)
    })
  },
  handleCardTextChange: function(e) {
    this.setState({cardText: e.target.value});
  },
  render: function() {
    return(
      <form id="card-search-bar" method="post" action="/search" role="search" onSubmit={this.handleSubmit}>
        <input type="hidden" name="authenticity_token" value={this.props.authenticity_token} />
        <div className="form-group">
          <div className="input-group">
            <input className="form-control" id="card-text" type="text" name="card_text" value={this.state.cardText} onChange={this.handleCardTextChange} />
            <span className="input-group-btn">
              <button type="submit" className="btn btn-default" ><span className="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div>
        </div>
      </form>
    )
  }
});
