var ColumnedCheckboxGroup = React.createClass({
  checkboxComponents: function() {
    return this.props.data.reduce(function(prev, curr, idx, array) {
      if(idx % 3 == 0) {
        return (prev.push(array.slice(idx, idx+3)), prev);
      }
      else {
        return prev;
      }
    }, []).map(function(set_of_rows) {
      return (
        <div className="col-xs-12 col-md-4">
          {this.checkboxSet(set_of_rows)}
        </div>
      );
    }, this);
  },
  checkboxSet: function(set_of_data) {
    return set_of_data.map(function(row) {
      return (
        <label key={"label-" + row.sanitized_value} className="checkbox">
          <input key={"checkbox-" + row.sanitized_value} id={this.props.id + "-" + row.sanitized_value} type="checkbox" name={this.props.name} value={row.value} /> {row.label}
        </label>
      );
    }, this);
  },
  render: function() {
    return (
      <div className="form-group" id={this.props.id}>
        <fieldset>
          <legend>{this.props.label}</legend>
          {this.checkboxComponents()}
        </fieldset>
      </div>
    );
  }
});
