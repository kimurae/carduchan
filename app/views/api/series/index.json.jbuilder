json.series series do |serie|
  json.name serie.name
  json.react_id "series-#{serie.id}"
  json.url api_series_cards_path(serie, format: :json)
end
