
json.set! :card_facets do

  %i(card_types colors costs levels powers souls traits).each do |key|

    json.set! key do
      json.array! card_facets[key] do |record|
        json.value           record
        json.label           record.to_s.titleize
        json.sanitized_value record.to_s.parameterize(separator: '-')
      end
    end
  end

  json.expansions(card_facets[:expansions]) do |expansion|
    json.value            expansion.id
    json.label            expansion.name.titleize
    json.sanitized_value  expansion.name.parameterize(separator: '-')
  end

  json.rarities(card_facets[:rarities]) do |rarity|
    json.value rarity
    json.label rarity
    json.sanitized_value rarity.parameterize(separator: '-')
  end
end
