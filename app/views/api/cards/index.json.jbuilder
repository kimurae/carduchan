json.cards cards do |card|
  json.react_id ['card', card.first_printing_number].join('-').parameterize(separator: '-')

  json.extract! card, :card_type, :color, :cost, :expansion_name, :image_url, :level, :name, :power, :soul

  json.text make_pretty(card.text)

  json.printings card.printings.sort_by(&:number) do |printing|
    json.extract! printing, :number, :rarity
    json.react_id ['printing', printing.number].join('-').parameterize(separator: '-')
  end

  json.traits card.traits.each_with_index.to_a do |(trait,idx)|
    json.react_id ['card-trait', card.first_printing_number, idx.to_s].join('-').parameterize(separator: '-')
    json.trait trait.titleize
  end

  json.triggers card.triggers.each_with_index.to_a do |(trigger,idx)|
    json.react_id ['card-trigger', card.first_printing_number, idx.to_s].join('-').parameterize(separator: '-')
    json.trigger trigger.titleize.upcase
  end
end
