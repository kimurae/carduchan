source 'https://rubygems.org'

ruby '3.2.2'

gem 'rails', '~> 6.0'

gem 'jbuilder', '~> 2.0'    # JSON API
gem 'pg',       '~> 1.5'   # Postgres Driver
gem 'puma',     '~> 6.0'    # Web Server

# Assets
gem 'bootstrap-sass', '~> 3.3.6' # Bootstrap CSS Framework (SASS ver.)
gem 'jquery-rails',   '~> 4.3.1' # JQuery Framework
gem 'react-rails',    '~> 1.11.0' # ReactJS Framework
gem 'sass-rails',     '~> 5.0'   # Sass transpiler, and css compressor.
gem 'uglifier',       '>= 1.3.0' # Compresses Javascript

# Development Tools
gem 'byebug', platform: :mri, group: %i(development test)
gem 'foreman',                group: :development
gem 'listen',                 group: :development
gem 'web-console',            group: :development

# Tests
gem 'factory_girl_rails', '~> 4.7.0', group: %i(development test)
gem 'jasmine',            '~> 2.4.0', group: %i(development test)
gem 'rspec-rails',        '~> 3.7',   group: %i(development test)

# Documentation
gem 'yard',               group: :development
gem 'yard-activerecord',  group: :development

# Native gems
gem 'nio4r', '2.5.2'     # Used by ActionCable, Puma
gem 'nokogiri', '1.14.3' # Used by Rails (And every XML thing in Ruby)

group :test do
  gem 'capybara',           '~> 2.7.1' # Feature Specs
  gem 'database_cleaner',   '~> 1.5'
  gem 'json-schema',        '~> 2.6.2'
  gem 'selenium-webdriver', '~> 3.7.0' # Javascript driver
end

# Heroku
gem 'newrelic_rpm',   group: :production
gem 'rails_12factor', group: :production
