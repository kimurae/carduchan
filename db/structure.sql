--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: card_triggers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE card_triggers (
    id integer NOT NULL,
    come_back integer DEFAULT 0 NOT NULL,
    draw integer DEFAULT 0 NOT NULL,
    gate integer DEFAULT 0 NOT NULL,
    pool integer DEFAULT 0 NOT NULL,
    return integer DEFAULT 0 NOT NULL,
    shot integer DEFAULT 0 NOT NULL,
    soul integer DEFAULT 0 NOT NULL,
    treasure integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: card_triggers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE card_triggers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: card_triggers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE card_triggers_id_seq OWNED BY card_triggers.id;


--
-- Name: cards; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cards (
    id integer NOT NULL,
    series_id integer,
    image_url character varying,
    name character varying DEFAULT 'Unknown'::character varying NOT NULL,
    text text,
    traits character varying[] DEFAULT '{}'::character varying[],
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    metadatum_id integer NOT NULL,
    expansion_id integer NOT NULL,
    card_trigger_id integer NOT NULL,
    first_printing_number character varying
);


--
-- Name: cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cards_id_seq OWNED BY cards.id;


--
-- Name: expansions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE expansions (
    id integer NOT NULL,
    name character varying DEFAULT 'Unknown'::character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: expansions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE expansions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: expansions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE expansions_id_seq OWNED BY expansions.id;


--
-- Name: metadata; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE metadata (
    id integer NOT NULL,
    card_type character varying DEFAULT 'Unknown'::character varying NOT NULL,
    color character varying DEFAULT 'unknown'::character varying NOT NULL,
    level integer DEFAULT 0 NOT NULL,
    soul integer DEFAULT 0 NOT NULL,
    power integer DEFAULT 0 NOT NULL,
    cost integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE metadata_id_seq OWNED BY metadata.id;


--
-- Name: printings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE printings (
    id integer NOT NULL,
    card_id integer NOT NULL,
    image_url character varying,
    number character varying NOT NULL,
    rarity character varying DEFAULT 'Unknown'::character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    booster boolean DEFAULT false,
    extra_booster boolean DEFAULT false,
    promo boolean DEFAULT false,
    trial_deck boolean DEFAULT false
);


--
-- Name: printings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE printings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: printings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE printings_id_seq OWNED BY printings.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: series; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE series (
    id integer NOT NULL,
    name character varying
);


--
-- Name: series_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE series_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: series_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE series_id_seq OWNED BY series.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY card_triggers ALTER COLUMN id SET DEFAULT nextval('card_triggers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cards ALTER COLUMN id SET DEFAULT nextval('cards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY expansions ALTER COLUMN id SET DEFAULT nextval('expansions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY metadata ALTER COLUMN id SET DEFAULT nextval('metadata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY printings ALTER COLUMN id SET DEFAULT nextval('printings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY series ALTER COLUMN id SET DEFAULT nextval('series_id_seq'::regclass);


--
-- Name: cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: documents; Type: MATERIALIZED VIEW; Schema: public; Owner: -; Tablespace: 
--

CREATE MATERIALIZED VIEW documents AS
 SELECT cards.id AS card_id,
    (((setweight(to_tsvector('english'::regconfig, (cards.name)::text), 'A'::"char") || setweight(to_tsvector('english'::regconfig, array_to_string(ARRAY( SELECT printings.number
           FROM printings
          WHERE (printings.card_id = cards.id)), ','::text)), 'A'::"char")) || setweight(to_tsvector('english'::regconfig, array_to_string(cards.traits, ','::text)), 'B'::"char")) || setweight(to_tsvector('english'::regconfig, cards.text), 'C'::"char")) AS document
   FROM cards
  GROUP BY cards.id
  WITH NO DATA;


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: card_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY card_triggers
    ADD CONSTRAINT card_triggers_pkey PRIMARY KEY (id);


--
-- Name: expansions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY expansions
    ADD CONSTRAINT expansions_pkey PRIMARY KEY (id);


--
-- Name: metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- Name: printings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY printings
    ADD CONSTRAINT printings_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: series_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY series
    ADD CONSTRAINT series_pkey PRIMARY KEY (id);


--
-- Name: idx_card_triggers; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_card_triggers ON card_triggers USING btree (come_back, draw, gate, pool, return, shot, soul, treasure);


--
-- Name: idx_fts_documents; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_fts_documents ON documents USING gin (document);


--
-- Name: idx_metadata; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX idx_metadata ON metadata USING btree (card_type, color, level, soul, power, cost);


--
-- Name: index_cards_on_series_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_cards_on_series_id ON cards USING btree (series_id);


--
-- Name: index_documents_on_card_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_documents_on_card_id ON documents USING btree (card_id);


--
-- Name: index_expansions_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_expansions_on_name ON expansions USING btree (name);


--
-- Name: index_printings_on_card_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_printings_on_card_id ON printings USING btree (card_id);


--
-- Name: index_printings_on_number; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_printings_on_number ON printings USING btree (number);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20160604025113'), ('20160604040852'), ('20160604133543'), ('20160604153418'), ('20160604173054'), ('20160604182818'), ('20160605013550'), ('20160610021529'), ('20160615020111');


