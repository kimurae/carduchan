class CreatePrintings < ActiveRecord::Migration[5.0]
  def change
    create_table :printings do |t|
      t.integer :card_id,   null: false, index: true
      t.string :image_url
      t.string :number,     null: false
      t.string :rarity,     null: false, default: 'Unknown'

      t.index :number, unique: true
      t.timestamps
    end

    change_column :cards, :name, :string, null: false, default: 'Unknown', index: true
  end
end
