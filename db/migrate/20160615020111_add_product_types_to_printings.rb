class AddProductTypesToPrintings < ActiveRecord::Migration[5.0]
  def change
    add_column :printings, :booster, :boolean, default: false
    add_column :printings, :extra_booster, :boolean, default: false
    add_column :printings, :promo, :boolean, default: false
    add_column :printings, :trial_deck, :boolean, default: false
  end
end
