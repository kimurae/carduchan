class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards do |t|
      t.integer :series_id, index: true
      t.string  :image_url
      t.string  :name
      t.integer :power
      t.text    :text
      t.string  :traits, array: true, default: []

      t.timestamps
    end
  end
end
