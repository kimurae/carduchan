class CreateCardTriggers < ActiveRecord::Migration[5.0]
  def change
    create_table :card_triggers do |t|
      t.integer  :come_back, default: 0, null: false
      t.integer  :draw,      default: 0, null: false
      t.integer  :gate,      default: 0, null: false
      t.integer  :pool,      default: 0, null: false
      t.integer  :return,    default: 0, null: false
      t.integer  :shot,      default: 0, null: false
      t.integer  :soul,      default: 0, null: false
      t.integer  :treasure,  default: 0, null: false

      t.index %i(come_back draw gate pool return shot soul treasure), name: 'idx_card_triggers', unique: true

      t.timestamps
    end

    add_column :cards, :card_trigger_id, :integer, null: false, index: true
  end
end
