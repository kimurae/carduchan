class CreateSearchView < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL

      CREATE MATERIALIZED VIEW documents AS
      SELECT cards.id AS card_id,
             setweight(to_tsvector('english', cards.name), 'A') ||
             setweight(to_tsvector('english', array_to_string(ARRAY(SELECT printings.number FROM printings WHERE printings.card_id = cards.id), ',')), 'A') ||
             setweight(to_tsvector('english', array_to_string(cards.traits,',')), 'B') ||
             setweight(to_tsvector('english', cards.text), 'C') AS document
        FROM cards
      GROUP BY cards.id;

      CREATE INDEX idx_fts_documents ON documents USING gin(document);
    SQL

      add_index :documents, :card_id

  end

  def down
    remove_index :documents, :card_id

    execute <<-SQL
      DROP INDEX idx_fts_documents;
      DROP MATERIALIZED VIEW documents;
    SQL
  end
end
