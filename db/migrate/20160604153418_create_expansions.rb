class CreateExpansions < ActiveRecord::Migration[5.0]
  def change
    create_table :expansions do |t|
      t.string :name, null: false, default: 'Unknown'
      t.timestamps
      t.index :name, unique: true
    end

    add_column :cards, :expansion_id, :integer, null: false, index: true
  end
end
