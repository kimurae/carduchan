class AddFirstPrintingNumberToCards < ActiveRecord::Migration[5.0]
  def change
    add_column :cards, :first_printing_number, :string
  end
end
