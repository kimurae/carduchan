class CreateMetadata < ActiveRecord::Migration[5.0]
  def change
    create_table :metadata do |t|
      t.string  :card_type, null: false, default: 'Unknown'
      t.string  :color,     null:false, default: 'unknown'
      t.integer :level,     null:false, default: 0
      t.integer :soul,      null:false, default: 0
      t.integer :power,     null:false, default: 0
      t.integer :cost,      null:false, default: 0
      t.index [:card_type, :color, :level, :soul, :power, :cost], unique: true, name: 'idx_metadata'
      t.timestamps
    end

    add_column :cards, :metadatum_id, :integer, null: false, index: true

    remove_column :cards, :power
  end
end
