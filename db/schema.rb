# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160615020111) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "card_triggers", id: :serial, force: :cascade do |t|
    t.integer "come_back", default: 0, null: false
    t.integer "draw", default: 0, null: false
    t.integer "gate", default: 0, null: false
    t.integer "pool", default: 0, null: false
    t.integer "return", default: 0, null: false
    t.integer "shot", default: 0, null: false
    t.integer "soul", default: 0, null: false
    t.integer "treasure", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["come_back", "draw", "gate", "pool", "return", "shot", "soul", "treasure"], name: "idx_card_triggers", unique: true
  end

  create_table "cards", id: :serial, force: :cascade do |t|
    t.integer "series_id"
    t.string "image_url"
    t.string "name", default: "Unknown", null: false
    t.text "text"
    t.string "traits", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "metadatum_id", null: false
    t.integer "expansion_id", null: false
    t.integer "card_trigger_id", null: false
    t.string "first_printing_number"
    t.index ["series_id"], name: "index_cards_on_series_id"
  end

  create_table "expansions", id: :serial, force: :cascade do |t|
    t.string "name", default: "Unknown", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_expansions_on_name", unique: true
  end

  create_table "metadata", id: :serial, force: :cascade do |t|
    t.string "card_type", default: "Unknown", null: false
    t.string "color", default: "unknown", null: false
    t.integer "level", default: 0, null: false
    t.integer "soul", default: 0, null: false
    t.integer "power", default: 0, null: false
    t.integer "cost", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_type", "color", "level", "soul", "power", "cost"], name: "idx_metadata", unique: true
  end

  create_table "printings", id: :serial, force: :cascade do |t|
    t.integer "card_id", null: false
    t.string "image_url"
    t.string "number", null: false
    t.string "rarity", default: "Unknown", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "booster", default: false
    t.boolean "extra_booster", default: false
    t.boolean "promo", default: false
    t.boolean "trial_deck", default: false
    t.index ["card_id"], name: "index_printings_on_card_id"
    t.index ["number"], name: "index_printings_on_number", unique: true
  end

  create_table "series", id: :serial, force: :cascade do |t|
    t.string "name"
  end

end
